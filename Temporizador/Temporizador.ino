// Nombres para los los puertos.
int ledInterno = 13;
int led = 12;
int pulsador = 2;

//Temporizadores
unsigned long time;
unsigned long temporizador;
unsigned long temporizadorDescanso;

//Tiempo de encendido cada 5 segundos.
int tiempoActivoEnSg = 5;
int tiempoActivoEnMsg = 1000 * tiempoActivoEnSg;
int tiempoDescansoEnSg = 1;
int tiempoDescansoEnMsg = 1000 * tiempoDescansoEnSg;

int tiempoLedEncendido = 1000 * 1; //1sg

// Configuracion inicial
void setup() {    
  //Activamos la serializacion
  Serial.begin(9600);  

  inicializadorTiempos();

  // inicialziacion de los puertos a salida
  pinMode(ledInterno, OUTPUT);     
  pinMode(led, OUTPUT);   
  pinMode(pulsador, INPUT);  
}

void inicializadorTiempos(){
  //Control del tiempo
  temporizador = millis() + tiempoActivoEnMsg - tiempoLedEncendido;
  temporizadorDescanso = temporizador + tiempoDescansoEnMsg - tiempoLedEncendido;  
}

// bucle infinito
void loop() {
  time = millis(); 

  int estadoPulsador = digitalRead(pulsador);
  if(estadoPulsador == HIGH){
    inicializadorTiempos();
    escribirEnMonitorPulsador();
  }

  int isTiempoConsumido =   time >= temporizador;  
  if(isTiempoConsumido){
    ejecutarAccionActivo();
  }
  else{
    int isTiempoConsumido =   time >= temporizadorDescanso;  
    if(isTiempoConsumido){
      ejecutarAccionDescanso();
    }
  }
}

void ejecutarAccionActivo(){
    temporizador = time + actualizacionTiempo();
    activarLed(led);  
    escribirEnMonitor(1);
}

void ejecutarAccionDescanso(){
    temporizadorDescanso = time + actualizacionTiempo();
    activarLed(ledInterno);  
    escribirEnMonitor(0);
}

int actualizacionTiempo(){
  return (tiempoDescansoEnMsg + tiempoActivoEnMsg - tiempoLedEncendido);
}


void escribirEnMonitor(int estadoActivo){
  Serial.print("Time: ");
  Serial.print((estadoActivo)?"Activo":"Descanso");
  Serial.print("  ");
  Serial.print(time);
  Serial.print("  ");
  Serial.println(temporizador);  
}

void escribirEnMonitorPulsador(){
  Serial.println("***");
  Serial.println("Pulsador activo");
  Serial.println("***");
}


void activarLed(int puerto){
  digitalWrite(puerto, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);               // wait for a second
  digitalWrite(puerto, LOW);    // turn the LED off by making the voltage LOW  
}

